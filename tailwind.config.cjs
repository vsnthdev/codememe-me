/* eslint-env node */
/*
 *  Tailwind CSS configuration file for codememe.me
 *  Created On 24 July 2021
 */

const colors = require('tailwindcss/colors')

module.exports = {
    purge: ['**/*.html', '**/*.svg'],
    darkMode: false,
    theme: {
        extend: {
            colors: {
                gray: colors.gray,
            },
        },
        container: {
            center: true,
        },
        fontFamily: {
            body: [
                'Lexend',
                'ui-sans-serif',
                'system-ui',
                '-apple-system',
                'BlinkMacSystemFont',
                '"Segoe UI"',
                'Roboto',
                '"Helvetica Neue"',
                'Arial',
                '"Noto Sans"',
                'sans-serif',
            ],
        },
    },
    variants: {
        extend: {},
    },
    plugins: [],
}
